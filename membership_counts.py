import ldap

cscLdapUri = "ldap://ldap1.csclub.uwaterloo.ca ldap://ldap2.csclub.uwaterloo.ca"
cscPeopleBase = 'ou=People,dc=csclub,dc=uwaterloo,dc=ca'
cscLdap = ldap.initialize(cscLdapUri)
cscLdap.simple_bind_s("", "")

def membersFor(term, year):
    members = cscLdap.search_s(cscPeopleBase, ldap.SCOPE_SUBTREE,
        '(&(objectClass=member)(term=%s%d))' % (term, year))
    return len(members)

for year in [2019, 2020, 2021, 2022]:
    for term in ['w', 's', 'f']:
        count = membersFor(term, year)
        print("%s%d, %d" % (term, year % 2000, count))
